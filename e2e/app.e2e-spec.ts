import { Hero4Page } from './app.po';

describe('hero4 App', function() {
  let page: Hero4Page;

  beforeEach(() => {
    page = new Hero4Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
